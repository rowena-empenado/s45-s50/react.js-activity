import { Fragment } from "react";
import Banner from "../components/Banner";

// Activity
/* export default function Error() {

    return(
        <Fragment>
            <Banner/>
        </Fragment>
    )


} */

export default function Error() {
  const data = {
    title: "404 - Not Found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back Home",
  };

  return <Banner data={data} />;
}
