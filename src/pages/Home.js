import { Fragment } from "react";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

// Activity
/* export default function Home() {

    return(
        <Fragment>
            <Banner/>
            <Highlights/>
        </Fragment>
    )
 
} */

export default function Home() {
  const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere",
    destination: "/courses",
    label: "Enroll Now!",
  };

  return (
    <Fragment>
      <Banner data={data} />
      <Highlights />
    </Fragment>
  );
}
