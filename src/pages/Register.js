import { useEffect, useState, useContext } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import { Form, Button } from "react-bootstrap";
import UserContext from "../Usercontext";
import Swal from "sweetalert2";

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    fetch("http://localhost:4000/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please provide a different email.",
          });
          navigate("/register");
        } else {
          fetch("http://localhost:4000/users/register", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobile,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data) {
                Swal.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Welcome to Zuitt! You may now log in.",
                });

                navigate("/login");
              } else {
                Swal.fire({
                  title: "Registration Failed",
                  icon: "error",
                  text: "Something went wrong. Please try again.",
                });

                navigate("/register");
              }
            });
        }
      });
  }

  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      mobile !== "" &&
      mobile.length === 11 &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, email, mobile, password1, password2]);

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form className="mt-3" onSubmit={(e) => registerUser(e)}>
      <Form.Group className="mb-3" controlId="userFirstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter first name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userLastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter last name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="userMobile">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter mobile number"
          value={mobile}
          onChange={(e) => setMobile(e.target.value)}
          required
        />
        <Form.Text className="text-muted">Must be 11 digits.</Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label> Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Register
        </Button>
      ) : (
        <Button variant="secondary" type="submit" id="submitBtn" disabled>
          Register
        </Button>
      )}
    </Form>
  );
}
