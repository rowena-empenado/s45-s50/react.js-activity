const coursesData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat. Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Python - Django",
        description: "Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat. Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Java - Springboot",
        description: "Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat. Lorem ipsum dolor sit amet, qui minim labore adipisicing minim sint cillum sint consectetur cupidatat.",
        price: 55000,
        onOffer: true
    }
]

export default coursesData;

