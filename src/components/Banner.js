import { Button, Row, Col } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";

/* export default function Banner() {

    // Activity
    const {id} = useParams();
    console.log(id)

    const routes = ["courses", "register", "login", "logout"]
    let routeCheck = routes.indexOf(id)
    console.log(routeCheck)

    return(

        <Row>
            <Col className='p-5'>
                {
                    ((id != null) && (routeCheck === -1)) ?
                    
                    <>
                        <h1> Page Not Found </h1>
                        <br></br>
                        <p> 
                            Go to <Link to="/">Home</Link> page instead.
                        </p>
                    </>
                    
                    :
                    <>
                        <h1> Zuitt Coding Bootcamp </h1>
                        <p> Opportunities for everyone, everywhere. </p>
                        <Button variant="primary"> Enroll Now! </Button>      
                    </>
                }
            </Col>
        </Row>
    )
} */

export default function Banner({ data }) {
  //console.log(data);

  const { title, content, destination, label } = data;

  return (
    <Row>
      <Col className="p-5">
        <h1>{title}</h1>
        <p>{content}</p>
        <Button variant="primary" as={Link} to={destination}>
          {label}
        </Button>
      </Col>
    </Row>
  );
}
