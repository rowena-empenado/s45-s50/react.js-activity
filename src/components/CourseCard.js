import { Row, Col, Card, Button } from "react-bootstrap";
//import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  const { name, description, price, _id } = courseProp;

  //console.log(courseProp)

  /* 
        Syntax:
            const [getter, setter] = useState(initialGetterValue)
    */
  /* const [count, setCount] = useState(0);
  const [seatCount, setSeatCount] = useState(30); */

  //   function enroll() {
  //     /* if(seatCount <= 0) {
  //             alert("No more seats.")
  //         } else {
  //             setCount(count + 1)
  //             setSeatCount(seatCount - 1)

  //             console.log('Enrollees' + (count+1))
  //             console.log('Seats remaining: ' + (seatCount-1))
  //         } */

  //     setCount(count + 1);
  //     setSeatCount(seatCount - 1);

  //     console.log("Enrollees" + (count + 1));
  //     console.log("Seats remaining: " + (seatCount - 1));
  //   }

  //   useEffect(() => {
  //     if (seatCount === 0) {
  //       alert("No more seats available.");
  //     }
  //   }, [seatCount]);

  return (
    <Row className="my-3">
      <Col>
        <Card className="">
          <Card.Body>
            <Card.Text className="course-title" /* as={Link} to="/courseview" */>
              {name}
            </Card.Text>
            <Card.Subtitle className="mt-3"> Description: </Card.Subtitle>
            <Card.Text> {description} </Card.Text>
            <Card.Subtitle> Price: </Card.Subtitle>
            <Card.Text> PhP {price} </Card.Text>
            {/* <Card.Subtitle> Enrollees: </Card.Subtitle>
            <Card.Text> {count} Enrollees </Card.Text>
            <Button variant="primary" onClick={enroll}>
              Enroll
            </Button> */}
            <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
